# How-To

## Development

### Project dependencies
This template is using `Pipenv` to manage the project dependencies. Please be sure that you have it installed on your machine.
If that is not the case, install it by following this guide: [https://pipenv.pypa.io/en/latest/installation.html](https://pipenv.pypa.io/en/latest/installation.html).

Once the Pipenv installation is completed, you can install the project dependencies from the provided `Pipfile` by running:
```
pipenv install
```
More information about the basic usage of Pipenv can be found here: [https://pipenv-fork.readthedocs.io/en/latest/basics.html#example-pipenv-workflow](https://pipenv-fork.readthedocs.io/en/latest/basics.html#example-pipenv-workflow).

### Database configuration
- Rename [`config.yml.sample`](https://gitlab.unige.ch/courses1/pt1/flask-project-template/-/blob/master/config.yml.sample) to `config.yml`
- Add the configuration parameters to `config.yml`
