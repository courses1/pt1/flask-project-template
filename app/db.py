import mysql.connector
from flask import g, current_app


def get_db():
    if 'db' not in g:
        g.db = mysql.connector.connect(
            host=current_app.config["HOST"],
            user=current_app.config["USER"],
            password=str(current_app.config["PASS"]),
            database=current_app.config["DB"],
            port=current_app.config["PORT"]
        )

    return g.db


def close_db(e=None):
    db = g.pop('db', None)

    if db is not None:
        db.close()


def init_app(app):
    app.teardown_appcontext(close_db)
