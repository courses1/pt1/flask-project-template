import yaml
from yaml.loader import SafeLoader


class Config(object):
    TESTING = False


class ProductionConfig(Config):
    config_file_path = "config.yml"
    with open(config_file_path, "r") as yaml_file:
        config = yaml.load(yaml_file, Loader=SafeLoader)
        HOST = config["DATABASE"]["host"]
        USER = config["DATABASE"]["user"]
        PASS = config["DATABASE"]["password"]
        DB = config["DATABASE"]["database"]
        PORT = config["DATABASE"]["port"]


class TestConfig(Config):
    TESTING = True
    ENV = "test"
    USER = "user"
    PASS = "test_password"
    DB = "db_test"
